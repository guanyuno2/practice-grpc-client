using Grpc.Core;
using Grpc.Net.Client;
using PracticeGrpcServer.Protos.Shared.Interfaces;

namespace practice_grpc_client.Grpc.SharedGrpc;

public class CouponSharedService
{
    private readonly GrpcChannel _channel;
    private readonly CouponService.CouponServiceClient _client;

    public CouponSharedService(GrpcChannel channel)
    {
        _channel = channel;
        _client = new CouponService.CouponServiceClient(_channel);
    }

    // public override async Task<CouponReply> ReserveCoupon(CouponRequest request, CallOptions options)
    // {
    //     var reply = await _client.ReserveCouponAsync(new CouponRequest()
    //     {
    //         CouponToken = "test"
    //     });
    //     return reply;
    // }
}