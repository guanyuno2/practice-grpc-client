﻿FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /src
COPY ["practice-grpc-client.csproj", "./"]
RUN dotnet restore "practice-grpc-client.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "practice-grpc-client.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "practice-grpc-client.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "practice-grpc-client.dll"]
